//task-1
let number = prompt(`please input any number`);
if (number% 3 == 0 && number % 5 == 0) {
 console.log(`This number can be divided by 3 and 5`);
}
else {
 console.log(`This number cannot be divided by 3 and 5`);
}

//task-2

let N = prompt(`Please input any number`);
if (N % 2 == 0) {
 console.log(`This number is even`);
}
else {
 console.log(`This number is odd`);
}

//task-3

let arr = [5, 6, 4, 3, 2, 8, 13, 0, 8, -5, -11];
arr.sort((a, b) => a - b);
console.log(arr);

//task-4

let arr_1 = ['a', 'b', 'c', 'd'];
let arr_2 = ['a', 'b', 'c',];
let arr_3 = ['a', 'd'];
let arr_4 = ['a', 'b', 'k', 'e', 'e'];
let unique_arr = [...new Set(arr_1.concat(arr_2, arr_3, arr_4))];
console.log(unique_arr);

//task-5

const a = { job: null, age: 42 };
const b = { job: null, age: 42 };

objectsEqual(a, b); // true
objectsEqual(a, { age: 43 }); // false

function objectsEqual(a, b) {
  const entries1 = Object.entries(a);
  const entries2 = Object.entries(b);
  if (entries1.length !== entries2.length) {
    return false;
  }
  for (let i = 0; i < entries1.length; ++i) {
    // Keys
    if (entries1[i][0] !== entries2[i][0]) {
      return false;
    }
    // Values
    if (entries1[i][1] !== entries2[i][1]) {
      return false;
    }
  }

  return true;
}
console.log(objectsEqual(a, b))